#
#set -x
#set -d
#set -v


COPTS="  -fPIC -D_DEBUG=1 -D__TRACE__=1"
COPTS+=" -I ../../../utility_routines/ -I ../"


SRCS="  svpn_server.c ../svpn_common.c ../sha1.c"
SRCS+=" ../../../utility_routines/utility_routines.c"

LIBS="-pthread -static"

EXE="svpn_server"

build	()
{
	echo	"Compile with $1 gcc for $2 ..."

	$1gcc $COPTS -o $EXE-$2 -w -D__ARCH__NAME__=\"$2\" $SRCS $LIBS
}

	build	"arm-linux-gnueabihf-"		"ARMhf"
	#build	"arm-linux-gnueabi-"		"ARMel"
	#build	"mips-linux-gnu-"		"MIPS32"
	#build	"mipsel-linux-gnu-"		"MIPSel"
	build	""				"x86_64"
